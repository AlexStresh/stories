FansPen - draw a new way
========
Что это будет?
--------------
Веб-сайт "FansPen"- иновационная платформа для создания, просмотра и оценивания фанфиков.

Средства разработки
-------------------
Сервисы:
- Cloudinary;
- Microsoft Azure;
- Google, Facebook, Twitter, VK APIs.

Языки программирования:
- C#
- JavaScript.
- CSS;
- HTML;

Библиотеки:
- SignalR;
- jQuery;
- ASP.Net core 2
- Bootsrap
- iTextSharp
- SimpleMDE

Базы данных:
- MS SQL server

