﻿using Yandex.Money.Api.Sdk.Net;

namespace FansPen.Web.Services
{
    public class YandexMoneyAuthenticator : DefaultAuthenticator
    {
        public override string Token { get; set; }
    }
}