﻿using System;
using Yandex.Money.Api.Sdk.Interfaces;

namespace FansPen.Web.Services
{
    public class YandexMoneyHostProvider : IHostProvider
    {
        private const string ApiHost = @"https://money.yandex.ru";

        public Uri BuildUri(string relativeUri)
        {
            return new Uri(new Uri(ApiHost), relativeUri);
        }

        public Uri AuthorizationdUri => new Uri(new Uri(ApiHost), @"oauth/authorize");
    }
}