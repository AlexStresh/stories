﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FansPen.Web.Helpers
{
    public static class UriHelper
    {
        public static string BuildQuery(IDictionary<string, string> parameters)
        {
            var resultStringSeed = string.Empty;

            return parameters.Aggregate(resultStringSeed,
                    (resultString, item) => $"{resultString}&{Uri.EscapeDataString(item.Key)}={Uri.EscapeDataString(item.Value)}").Trim(new[] { '&' });
        }
    }
}