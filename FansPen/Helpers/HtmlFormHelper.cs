﻿using System.Collections.Generic;
using System.Text;

namespace FansPen.Web.Helpers
{
    public class HtmlFormHelper
    {
        private const string FORM = @"<form name=""form1"" method=""post"" action=""{0}"" >";
        private const string DATA = @"<input name=""{0}"" type=""hidden"" value=""{1}"">";
        private const string HEADER = @"<html><body onload=""document.form1.submit()"">";
        private const string FOOTER = @"</form></body></html>";


        private readonly Dictionary<object, object> _dictionary = new Dictionary<object, object>();

        public string Action { get; set; }
        public void Add(object key, object value)
        {
            _dictionary.Add(key, value);
        }


        public string BuildForm()
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append(HEADER);
            stringBuilder.AppendFormat(FORM, Action);
            foreach (object key in _dictionary.Keys)
                stringBuilder.AppendFormat(DATA, key, _dictionary[key]);
            stringBuilder.Append(FOOTER);
            return stringBuilder.ToString();
        }

    }
}