﻿using System.Collections.Generic;
namespace FansPen.Web.Models.ViewModels
{
    public class ApplicationUserViewModel
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string RegistrationDate { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string AboutMe { get; set; }
        public string Sex { get; set; }
        public string AvatarUrl { get; set; }
        public string ProviderKey { get; set; }
        public bool IsAdmin { get; set; }
        public bool IsBan { get; set; }
        public bool CanSubscribe { get; set; }
        public bool IsSubscribed { get; set; }
        public string PaymentStatus { get; set; }
        public string WalletNumber { get; set; }


        public ICollection<FanficPreViewModel> Fanfics { get; set; }
    }
}
