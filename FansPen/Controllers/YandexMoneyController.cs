﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Threading.Tasks;
using FansPen.Domain.Models;
using FansPen.Domain.Repository;
using FansPen.Web.Helpers;
using FansPen.Web.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Yandex.Money.Api.Sdk.Authorization;
using Yandex.Money.Api.Sdk.Interfaces;
using Yandex.Money.Api.Sdk.Net;
using Yandex.Money.Api.Sdk.Requests;
using Yandex.Money.Api.Sdk.Responses;
using Yandex.Money.Api.Sdk.Utils;

namespace FansPen.Web.Controllers
{
    public class YandexMoneyController : Controller
    {
        public ApplicationUserRepository ApplicationUserRepository { get; set; }
        public PublisherSubscriberRepository PublisherSubscriberRepository { get; set; }
        public IConfiguration Configuration { get; set; }

        private static string ClientId = "ABBFCDFB5640F40C2FC6F11602CA50A876CB6C211D9F8E4835729BA456C45D33"; // ToDo: from configuration
        private static string RedirectUrl = "https://fanf.azurewebsites.net/YandexMoney/AuthorizeEndpoint";

        public YandexMoneyController(ApplicationContext context, IConfiguration configuration)
        {
            ApplicationUserRepository = new ApplicationUserRepository(context);
            PublisherSubscriberRepository = new PublisherSubscriberRepository(context);
            Configuration = configuration;
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Result(string notification_type, string operation_id, string label, string datetime, decimal amount, decimal withdraw_amount, string sender, string sha1_hash, string currency, bool codepro)
        {
            // ToDo: validate sha1_hash, amount, sender's ip address

            Guid.TryParse(label, out Guid linkedId);
            try
            {
                PublisherSubscriber entity = PublisherSubscriberRepository.GetById(linkedId);
                if (entity == null || string.IsNullOrEmpty(operation_id))
                    return StatusCode(500);

                entity.IsActive = !codepro;
                entity.PaymentId = operation_id;
                PublisherSubscriberRepository.Save();
                string walletNumber = entity.Publisher.WalletNumber;

                if (!string.IsNullOrEmpty(walletNumber))
                {
                    var link = Url.Action(nameof(ApproveTransfer), "YandexMoney", new { walletNumber }, HttpContext.Request.Scheme);
                    string adminEmail = "sashastresh@gmail.com"; // ToDo: from configuration

                    EmailService emailService = new EmailService(Configuration);
                    await emailService.SendEmailAsync(adminEmail, $"Pending transfer for {entity.Publisher.UserName}", $"<a style='color: green' target='_blank' href='{link}'> Approve transfer </a>");
                }
            }
            catch (Exception e)
            {
                Trace.TraceError(e.ToString());
                throw;
            }

            return Ok();
        }

        [HttpGet]
        public async Task<IActionResult> AuthorizeEndpoint(string code, string error, string error_description, string wallet)
        {
            if (string.IsNullOrEmpty(code))
                return Json(error);

            var hostsProvider = new YandexMoneyHostProvider();
            var httpClient = new DefaultHttpPostClient(hostsProvider, new YandexMoneyAuthenticator());

            var tokenRequest = new TokenRequest<TokenResult>(httpClient, new JsonSerializer<TokenResult>())
            {
                ClientId = ClientId,
                Code = code,
                RedirectUri = RedirectUrl
            };

            var tokenModel = await tokenRequest.Perform();
            if (string.IsNullOrEmpty(tokenModel?.Token))
                return Json(tokenModel);

            httpClient = new DefaultHttpPostClient(hostsProvider, new YandexMoneyAuthenticator { Token = tokenModel.Token });

            decimal amount = 5; // ToDo: from configuration
            string paymentComment = "Fanfics: Donate payment";

            if (!string.IsNullOrEmpty(wallet))
            {
                var transferStatus = await TransferFunds(httpClient, wallet, amount, paymentComment);
                return Json(new { transferStatus = transferStatus.ToString() });
            }

            return Json(tokenModel);
        }

        [HttpGet]
        public IActionResult ApproveTransfer(string walletNumber)
        {
            var hostsProvider = new YandexMoneyHostProvider();

            var authRequestParams = new AuthorizationRequestParams
            {
                ClientId = ClientId,
                RedirectUri = $"{RedirectUrl}/?wallet={walletNumber}",
                Scope = Scopes.Compose(new[] { Scopes.AccountInfo, Scopes.PaymentToAccount(walletNumber) })
            };

            string redirectUrl = string.Format("{0}?{1}", hostsProvider.AuthorizationdUri, UriHelper.BuildQuery(authRequestParams.GetParams()));

            return Redirect(redirectUrl);
        }


        [NonAction]
        private async Task<ResponseStatus> TransferFunds(IHttpClient client, string walletNumber, decimal amount, string paymentComment)
        {
            var paymentParams = new P2PRequestPaymentParams
            {
                AmountDue = amount.ToString(CultureInfo.InvariantCulture),
                To = walletNumber,
                Comment = paymentComment,
                Message = paymentComment
            };

            var requestPaymentRequest = new RequestPaymentRequest<RequestPaymentResult>(client, new JsonSerializer<RequestPaymentResult>())
            {
                PaymentParams = paymentParams.GetParams()
            };

            var requestPaymentResult = await requestPaymentRequest.Perform();
            if (requestPaymentResult.Status != ResponseStatus.Success)
                return requestPaymentResult.Status;

            var processPaymentRequest = new ProcessPaymentRequest<ProcessPaymentResult>(client, new JsonSerializer<ProcessPaymentResult>())
            {
                RequestId = requestPaymentResult.RequestID,
                MoneySource = "wallet"
            };

            var processPaymentResult = await processPaymentRequest.Perform();

            return processPaymentResult.Status;
        }

    }
}