﻿using System;
using AutoMapper;
using FansPen.Domain.Models;
using FansPen.Domain.Repository;
using FansPen.Web.Models.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using FansPen.Web.Helpers;
using Microsoft.AspNet.Identity;

namespace FansPen.Web.Controllers
{
    public class ProfileController : Controller
    {
        public ApplicationUserRepository ApplicationUserRepository;
        public FanficRepository FanficRepository;
        public TagRepository TagRepository;
        public PublisherSubscriberRepository PublisherSubscriberRepository;
        static private List<FanficPreViewModel> _resultList { get; set; }
        private const int SizeOfPackage = 10;

        public ProfileController(ApplicationContext context)
        {
            ApplicationUserRepository = new ApplicationUserRepository(context);
            FanficRepository = new FanficRepository(context);
            TagRepository = new TagRepository(context);
            PublisherSubscriberRepository = new PublisherSubscriberRepository(context);
        }

        [HttpGet]
        [Route("Profile")]
        public IActionResult Profile(string id)
        {
            var user = ApplicationUserRepository.GetApplicationUserById(id);
            var model = Mapper.Map<ApplicationUserViewModel>(user);
            var currentUser = ApplicationUserRepository.GetApplicationUserById(User.Identity.GetUserId());
            if (currentUser != null)
            {
                var subscription = PublisherSubscriberRepository.GetCollection(user.Id).FirstOrDefault(x => x.SubscriberId == currentUser.Id);
                bool isSubscribedOnUser = subscription != null;
                model.CanSubscribe = user.Id != currentUser.Id && !isSubscribedOnUser;
                model.IsSubscribed = isSubscribedOnUser;

                if (isSubscribedOnUser)
                    model.PaymentStatus = subscription.IsActive ? "Paid" : "Awaiting payment";
            }

            return View("Index", model);
        }


        [HttpGet]
        [Route("Subscribe")]
        public IActionResult Subscribe(string userId)
        {
            var currentUser = ApplicationUserRepository.GetApplicationUserById(User.Identity.GetUserId());
            var publisherUser = ApplicationUserRepository.GetApplicationUserById(userId);
            var subscribers = PublisherSubscriberRepository.GetSubscribers(publisherUser.Id);

            decimal amount = 5;
            string receiver = "410018280263950"; // ToDo: From configuration

            if (!subscribers.Contains(currentUser))
            {
                var publisherSubscriber = new PublisherSubscriber
                {
                    Id = Guid.NewGuid(),
                    Created = DateTime.UtcNow,
                    IsActive = false,
                    PaymentId = string.Empty,
                    Subscriber = currentUser,
                    Publisher = publisherUser
                };

                PublisherSubscriberRepository.Create(publisherSubscriber);
                PublisherSubscriberRepository.Save();

                string comment = $"Payment for subscription on {publisherUser.UserName}";
                var htmlFormHelper = new HtmlFormHelper { Action = "https://money.yandex.ru/quickpay/confirm.xml" };
                
                htmlFormHelper.Add("receiver", receiver);
                htmlFormHelper.Add("label", publisherSubscriber.Id);
                htmlFormHelper.Add("sum", amount.ToString(CultureInfo.InvariantCulture));
                htmlFormHelper.Add("paymentType", "PC");
                htmlFormHelper.Add("formcomment", comment);
                htmlFormHelper.Add("targets", $"Fanfics: {comment}");
                htmlFormHelper.Add("quickpay-form", "donate");

                return new ContentResult { Content = htmlFormHelper.BuildForm(), ContentType = "text/html" };
            }

            return Profile(userId);
        }

        [HttpPost]
        [Route("SetFirstName")]
        public void SetFirstName(string id, string value)
        {
            ApplicationUserRepository.SetFirstNameById(id, value);
        }

        [HttpPost]
        [Route("SetSecondName")]
        public void SetSecondName(string id, string value)
        {
            ApplicationUserRepository.SetSecondNameById(id, value);
        }

        [HttpPost]
        [Route("SetSex")]
        public void SetSex(string id, string value)
        {
            ApplicationUserRepository.SetSexById(id, value);
        }

        [HttpPost]
        [Route("SetWalletNumber")]
        public void SetWalletNumber(string id, string value)
        {
            ApplicationUserRepository.SetWalletNumberById(id, value);
        }

        [HttpPost]
        [Route("SetAboutMe")]
        public void SetAboutMe(string id, string value)
        {
            ApplicationUserRepository.SetAboutMeById(id, value);
        }

        [HttpGet]
        [Route("GetUserFanfics")]
        public IActionResult GetUserFanfics(string id, string category, int sort)
        {
            var currentUser = ApplicationUserRepository.GetApplicationUserById(User.Identity.GetUserId());

            _resultList = new List<FanficPreViewModel>();
            _resultList = (category == "All") ?
                Mapper.Map<List<FanficPreViewModel>>(FanficRepository.GetUserFanfics(id, sort).Where(fanfic => PublisherSubscriberRepository.HasAccessTo(fanfic, currentUser)).ToList()) :
                Mapper.Map<List<FanficPreViewModel>>(FanficRepository.GetUserFanficsByCategory(id, category, sort).Where(fanfic => PublisherSubscriberRepository.HasAccessTo(fanfic, currentUser)).ToList());
            List<TagViewModel> tags = Mapper.Map<List<TagViewModel>>(TagRepository.GetList());
            _resultList.ForEach(x => x.SetTags(tags));
            return Json(new { fanfics = _resultList.Take(SizeOfPackage) });
        }

        [HttpGet]
        [Route("GetNext")]
        public IActionResult GetNext(int package)
        { 
            return Json(new { fanfics = _resultList.Skip(package).Take(SizeOfPackage) });
        }
    }
}
