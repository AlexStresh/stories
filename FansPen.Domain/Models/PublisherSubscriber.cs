﻿using System;

namespace FansPen.Domain.Models
{
    public class PublisherSubscriber
    {
        public Guid Id { get; set; }
        public DateTime Created { get; set; }
        public bool IsActive { get; set; }
        public string PaymentId { get; set; }

        public ApplicationUser Publisher { get; set; }
        public string PublisherId { get; set; }

        public ApplicationUser Subscriber { get; set; }
        public string SubscriberId { get; set; }
    }
}