﻿using System;
using System.Collections.Generic;
using System.Linq;
using FansPen.Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace FansPen.Domain.Repository
{
    public class PublisherSubscriberRepository: BaseRepository<PublisherSubscriber>
    {
        private DbSet<PublisherSubscriber> _publisherSubscribers;

        public PublisherSubscriberRepository(ApplicationContext context) : base(context)
        {
            _publisherSubscribers = context.Set<PublisherSubscriber>();
        }

        public PublisherSubscriber GetById(Guid id)
        {
            return _publisherSubscribers
                .Include(x => x.Subscriber)
                .Include(x => x.Publisher)
                .FirstOrDefault(x => x.Id == id);
        }

        public List<PublisherSubscriber> GetCollection(string publisherId)
        {
            return _publisherSubscribers
                .Where(p => p.PublisherId == publisherId)
                .Include(x => x.Subscriber)
                .ToList();
        }

        public List<ApplicationUser> GetSubscribers(string publisherId)
        {
            return _publisherSubscribers
                .Where(p => p.PublisherId == publisherId)
                .Include(x => x.Subscriber)
                .Select(s => s.Subscriber)
                .ToList();
        }

        public List<ApplicationUser> GetPublishers(string subscriberId)
        {
            return _publisherSubscribers
                .Where(s => s.SubscriberId == subscriberId)
                .Include(x => x.Publisher)
                .Select(p => p.Publisher)
                .ToList();
        }

        // ToDo: it's totally incorrect place for this method
        public bool HasAccessTo(Fanfic fanfic, ApplicationUser user)
        {
            if (!fanfic.IsHidden)
                return true;

            if (user == null)
                return false;

            string userId = user.Id;
            string creatorId = fanfic.ApplicationUserId;
            if (userId.Equals(creatorId))
                return true;

            return _publisherSubscribers
                .Include(x => x.Publisher)
                .Include(x => x.Subscriber)
                .Any(x => x.Publisher.Id == creatorId && x.SubscriberId == userId && x.IsActive);
        }
    }
}